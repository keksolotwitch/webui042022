package com.geekbrains.lesson5.hw4;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TriangleTest {
    @Test
    void checkCalculationTest() throws Exception {
        Assertions.assertEquals(1.732, Triangle.calcArea(2, 2, 2), 0.001);
    }

    @Test
    void incorrectTriangleTest() {
        Assertions.assertThrows(Exception.class, () -> Triangle.calcArea(-2, 2, 2));
    }
}
